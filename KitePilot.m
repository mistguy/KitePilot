(* ::Package:: *)

BeginPackage["KitePilot`"]

getHamiltonian::usage =
        "getHamiltonian[group,kBases,matrixBases,vars]"

Begin["`Private`"]
getOrder[poly_,vars_]:=Max[Map[Length[CoefficientList[poly,#]]&,vars]];
kRepresentation[rotationMatrix_,kBases_,vars_]:=Module[{l,rotatedk,a,b,order},
order=Max[Map[getOrder[#,vars]&,kBases]];
l=Length[kBases];
rotatedk=Inverse[rotationMatrix].vars;
a=Map[Flatten[CoefficientList[#,vars,{order,order,order}]]&,kBases];
b=Map[Flatten[CoefficientList[#,vars,{order,order,order}]]&,kBases/.{vars[[1]]->rotatedk[[1]],vars[[2]]->rotatedk[[2]],vars[[3]]->rotatedk[[3]]}];
Table[Conjugate[a[[i]]].b[[j]],{i,l},{j,l}]
];
frobeniusProduct[A_,B_]:=Tr[ConjugateTranspose[A].B];
expandInMatrixBasis[matrix_,matrixBases_]:= Map[frobeniusProduct[#,matrix]/frobeniusProduct[#,#]&,matrixBases];
hRepresentation[representationMatrix_,matrixBases_,antilinear_:False]:=
If[antilinear,
Transpose[Map[expandInMatrixBasis[representationMatrix.Conjugate[#].Inverse[representationMatrix],matrixBases]&,matrixBases]],
Transpose[Map[expandInMatrixBasis[representationMatrix.#.Inverse[representationMatrix],matrixBases]&,matrixBases]]
];
hkRepresentation[g_,kBases_,matrixBases_,vars_]:=Simplify[KroneckerProduct[kRepresentation[g[["rotationMatrix"]],kBases,vars],
hRepresentation[g[["representationMatrix"]],matrixBases,g[["antilinear"]]]]];
(*Credit: the following function calculating intersection of vector spaces comes from https://mathematica.stackexchange.com/questions/60306/intersection-of-two-vector-spaces*)
getIntersection[l1_,l2_]:=Module[{n,ker,coeffs},ker=NullSpace[Transpose[Join[l1,l2]]];
n=Length[l1];
coeffs=Map[Function[v,v[[1;;n]]],ker];
Return[Map[Function[v,v.l1],coeffs]];];
getInvariantVectors[g_,kBases_,matrixBases_,vars_]:=Module[{i,invariantVectors},
invariantVectors=Simplify[NullSpace[hkRepresentation[g, kBases,matrixBases,vars]-IdentityMatrix[Length[kBases] Length[matrixBases]]]];
If[Not[Re[invariantVectors]===invariantVectors],Print["invariantVectors not real"]];
Return[invariantVectors]];
vectorToExpression[vector_,kBases_,matrixBases_]:=
Total[Flatten[Table[vector[[(i-1) Length[matrixBases]+j]] kBases[[i]] matrixBases[[j]],{i,Length[kBases]},{j,Length[matrixBases]}],1]];
getHamiltonian[group_,kBases_,matrixBases_,vars_]:=Module[{i,nullVectors,currentNullVectors},
nullVectors=getInvariantVectors[group[[1]],kBases,matrixBases,vars];
For[i=2,i<=Length[group],i++,
currentNullVectors=getInvariantVectors[group[[i]],kBases,matrixBases,vars];
nullVectors=getIntersection[nullVectors,currentNullVectors]
];
Map[vectorToExpression[#,kBases,matrixBases]&,nullVectors]
];
End[]
EndPackage[]



