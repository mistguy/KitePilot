<TeXmacs|1.99.8>

<style|<tuple|generic|old-spacing>>

<\body>
  <doc-data|<doc-title|Note for <name|KitePilot>>>

  Usually, <math|\<b-k\>\<cdot\>\<b-p\>> Hamiltonian is obtained by
  enumerating all possible matrices respecting every symmetry in the magnetic
  group. The symmetry constraint of a symmetry operator on the Hamiltonian
  <math|<wide|H|^>> is

  <\eqnarray*>
    <tformat|<table|<row|<cell|<wide|g|^><around*|(|\<b-k\>|)>*<wide|H|^><around*|(|\<b-k\>|)>*<wide|g|^><around*|(|\<b-k\>|)><rsup|-1>>|<cell|=>|<cell|<wide|H|^><around*|(|g\<circ\>\<b-k\>|)>,>>>>
  </eqnarray*>

  where\ 

  <\eqnarray*>
    <tformat|<table|<row|<cell|<wide|g|^><around*|(|\<b-k\>|)>>|<cell|=>|<cell|\<mathe\><rsup|-\<mathi\>*<around*|[|<around*|(|g\<circ\>\<b-k\>|)>\<cdot\>\<b-delta\>|]>>*<wide|g|^>>>>>
  </eqnarray*>

  and <math|\<b-delta\>> is the fractional translation part of <math|g> if
  <em|g> is nonsymmorphic. By the definition of <math|\<b-k\>\<cdot\>\<b-p\>>
  approximation, every operator is expanded in a <math|\<b-k\>> independent
  basis <math|<around*|\||u<rsub|n>\<rangle\>|\<nobracket\>>>:

  <\eqnarray*>
    <tformat|<table|<row|<cell|<wide|g|~>*K<rsup|s<around*|(|g|)>>
    H<around*|(|\<b-k\>|)> K<rsup|s<around*|(|g|)>>*<wide|g|~><rsup|\<dagger\>>>|<cell|=>|<cell|H<around*|(|g\<circ\>\<b-k\>|)>,<eq-number><label|sym-ham-mat>>>>>
  </eqnarray*>

  where <math|s<around*|(|g|)>=0 <around*|(|1|)>> denotes <math|g> is linear
  (antilinear) and

  <\eqnarray*>
    <tformat|<table|<row|<cell|<wide|g|~><rsub|m\<nocomma\>n>>|<cell|=>|<cell|<around*|\<langle\>|u<rsub|m><around*|\||<wide|g|^>|\<nobracket\>>*u<rsub|n>|\<rangle\>>.>>>>
  </eqnarray*>

  Notice that phase <math|\<mathe\><rsup|-\<mathi\>*<around*|[|<around*|(|g\<circ\>\<b-k\>|)>\<cdot\>\<b-delta\>|]>>>
  is cancelled in Eq. (<reference|sym-ham-mat>) and <math|<wide|g|~>> is
  <em|<strong|k>> independent.

  To find every matrix respecting Eq. (<reference|sym-ham-mat>), we expand
  <math|H<around*|(|\<b-k\>|)>> in a vector space
  <math|\<cal-H\>\<otimes\>\<cal-K\>>, where <math|\<cal-H\>> is the vector
  space of <math|N\<times\>N> Hermitian matrices and <math|\<cal-V\>> is the
  space of polynomials of <math|\<b-k\>>. The bases of <math|\<cal-K\>> can
  be naturally chosen as <math|<around*|{|1,k<rsub|x>,k<rsub|y>,k<rsub|z>,k<rsub|x><rsup|2>,k<rsub|y><rsup|2>,k<rsub|z><rsup|2>,k<rsub|x>*k<rsub|y>,k<rsub|y>*k<rsub|z>,k<rsub|x>*k<rsub|z>,\<cdots\>|}>>
  while the bases of <math|\<cal-H\>> are less obvious. We require the bases
  of <math|\<cal-H\>> are orthogonal to each other with respect to Frobenius
  product such that expansion in <math|\<cal-H\>> is trivial. We emphasize
  both <math|\<cal-H\>> and <math|\<cal-K\>> are over <math|\<bbb-R\>> such
  that complex conjugation acts trivially on the coefficients.

  We now define <math|g> act on <math|H<around*|(|\<b-k\>|)>> as\ 

  <\eqnarray*>
    <tformat|<table|<row|<cell|g\<ast\>H<around*|(|\<b-k\>|)>>|<cell|=>|<cell|<wide|g|~>*K<rsup|s<around*|(|g|)>>
    H<around*|(|g<rsup|-1>\<circ\>\<b-k\>|)>
    K<rsup|s<around*|(|g|)>>*<wide|g|~><rsup|\<dagger\>>>>>>
  </eqnarray*>

  such that Eq. (<reference|sym-ham-mat>) reduces to
  <math|g*\<ast\>H<around*|(|\<b-k\>|)>=H<around*|(|\<b-k\>|)>>. This action,
  decomposed in <math|\<cal-H\>\<otimes\>\<cal-K\>>, becomes\ 

  <\eqnarray*>
    <tformat|<table|<row|<cell|g\<ast\>H>|<cell|=>|<cell|<wide|g|~>*K<rsup|s<around*|(|g|)>>
    H K<rsup|s<around*|(|g|)>>*<wide|g|~><rsup|\<dagger\>>>>>>
  </eqnarray*>

  and\ 

  <\eqnarray*>
    <tformat|<table|<row|<cell|g\<ast\>\<b-k\>>|<cell|=>|<cell|g<rsup|-1>\<circ\>\<b-k\>.>>>>
  </eqnarray*>

  The representation matrix of <math|g> (denoted as
  <math|<wide|g|\<breve\>>>) in <math|\<cal-H\>\<otimes\>\<cal-K\>> is thus
  Kronecker product of representation matrices of <math|g> in
  <math|\<cal-H\>> and <math|\<cal-K\>>. Null space of
  <math|<wide|g|\<breve\>>-I> is exactly all matrices respecting Eq.
  (<reference|sym-ham-mat>) where <math|I> is identity matrix.
</body>

<\initial>
  <\collection>
    <associate|page-medium|automatic>
  </collection>
</initial>

<\references>
  <\collection>
    <associate|sym-ham-mat|<tuple|1|?>>
  </collection>
</references>